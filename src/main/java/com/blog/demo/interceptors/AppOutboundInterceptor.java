package com.blog.demo.interceptors;

import org.apache.cxf.ext.logging.LoggingInInterceptor;
import org.apache.cxf.ext.logging.LoggingOutInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.InInterceptors;
import org.apache.cxf.interceptor.OutInterceptors;
import org.apache.cxf.message.Message;

@OutInterceptors
public class AppOutboundInterceptor extends LoggingOutInterceptor {

    @Override
    public void handleMessage(final Message message) throws Fault {
        System.out.println("Wyjście");
        super.handleMessage(message);
    }
}
