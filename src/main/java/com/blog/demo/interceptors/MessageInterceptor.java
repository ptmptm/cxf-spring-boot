package com.blog.demo.interceptors;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.saaj.SAAJOutInterceptor;
import org.apache.cxf.ext.logging.LoggingInInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.InInterceptors;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.phase.PhaseInterceptor;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MessageInterceptor extends AbstractPhaseInterceptor<SoapMessage> {

    private List<PhaseInterceptor<? extends Message>> extras = new ArrayList<>(1);
    private XPath xPath = XPathFactory.newInstance().newXPath();

    public MessageInterceptor() {
        super(Phase.USER_PROTOCOL);
        extras.add(new SAAJOutInterceptor());
    }

    @Override
    public Collection<PhaseInterceptor<? extends Message>> getAdditionalInterceptors() {
        return extras;
    }

    @Override
    public void handleMessage(final SoapMessage soapMessage) throws Fault {

        SOAPMessage msg = soapMessage.getContent(SOAPMessage.class);

        try {
            SOAPBody soapBody = msg.getSOAPBody();
            soapBody.setPrefix("cas");//Dodaje do body prefix

            SOAPEnvelope envelope = msg.getSOAPPart().getEnvelope();
            envelope.setPrefix("cas");//Dodaje do envelope prefix

            envelope.removeNamespaceDeclaration("soap");
            envelope.addNamespaceDeclaration("fsdf", "fssssssss");

//            Element e = msg.getSOAPBody();
//            e.getFirstChild().setPrefix("cas");//Dodaje do pierwszego dziecka body prefix
        } catch (SOAPException e) {
            e.printStackTrace();
        }

    }
}
