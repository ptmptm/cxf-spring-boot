package com.blog.demo.interceptors;

import org.apache.cxf.ext.logging.LoggingInInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.InInterceptors;
import org.apache.cxf.message.Message;

@InInterceptors
public class AppInboundInterceptor extends LoggingInInterceptor {

    @Override
    public void handleMessage(final Message message) throws Fault {
        System.out.println("Wejście");
        super.handleMessage(message);
    }
}
